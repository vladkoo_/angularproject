import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {CarComponent} from './components/car/car.component';
import {ContactsComponent} from './components/contacts/contacts.component';
import {NgModule} from '@angular/core';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'car', component: CarComponent},
  {path: 'contacts/:id', component: ContactsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

