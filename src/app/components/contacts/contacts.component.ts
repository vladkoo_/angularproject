import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {PersonInterface} from '../interfaces/Person';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  myString = 'Hands up';
  id: string;
  @Input() person: PersonInterface;
  @Output() Grow = new EventEmitter();

  fireGrowEvent(event) {
    this.Grow.emit(event);
  }

  constructor(private route: ActivatedRoute) {
    this.id = route.snapshot.params.id;
  }

  ngOnInit() {
  }

}
