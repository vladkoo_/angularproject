import {Component, OnInit} from '@angular/core';
import {LoginService} from 'src/app/services/login.service';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {

  properties = {red: true, underline: true, bold: false};
  links = [
    {class: this.properties, route: 'car', name: 'car'},
    {class: '', route: 'contacts/777', name: 'contacts'},
  ];

  getAllUsers() {
    this.users.getListOfUsers().subscribe((data) => {
      console.log(data);
    });
  }

  constructor(private logger: LoginService,
              private users: UserService) {}

  sayHi() {
    this.logger.hi();
  }

  ngOnInit() {
  }

}
