import { Component, OnInit } from '@angular/core';
import {PersonInterface} from '../interfaces/Person';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css'],
})
export class CarComponent implements OnInit {
  name: string;
  model: string;
  speed: number;
  options: string[];
  isEdit: boolean;

  person: PersonInterface;

  bmwSelect(carname: string ) {
    if (carname === 'bmw') {
    alert('bmw');
    }
  }

  addOption(option: string) {
    console.log(option);
    this.options.unshift(option);
    return false;
  }

  deleteOption(index: number) {
    this.options.splice(index, 1);
  }

  showEdit() {
    this.isEdit = !this.isEdit;
  }

  grow(event) {
    alert('done');
    console.log(event);
  }

  constructor() { }

  ngOnInit() {
    this.name = 'BMW';
    this.model = 'X5';
    this.speed = 25;
    this.options = ['Abs', 'Autopilot', 'Auto parking'];
    this.isEdit = false;
    this.person = {
      name: 'Vladislav',
      age: 20
    };
  }
}
